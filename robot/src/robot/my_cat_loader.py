import rospy
import sys
from robot.msg import cat, cats


def main():
    rospy.init_node('load_cat_file')
    rate = rospy.Rate(10)

    pub = rospy.Publisher('cats', cats, queue_size=1)
    path = sys.path[0]
    path = '%s/../resources/my_cats.txt' % path

    file1 = open(path, "r")

    list1 = []
    colorList = []
    lines = file1.readlines()
    for i in range(1, len(lines)):
        list1.append(lines[i].split())
    for i in range(len(list1)):
        colorList.append(list1[i][2].split(','))

    cats1 = cats()

    for i in range(len(list1)):
        cat1 = cat()
        cat1.label = list1[i][0]
        cat1.color = colorList[i]
        cats1.cats.append(cat1)

    # print cats1
    while not rospy.is_shutdown():
        pub.publish(cats1)
        rate.sleep()
