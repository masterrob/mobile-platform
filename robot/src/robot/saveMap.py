import rospy
from sensor_msgs.msg import PointCloud2
from robot.msg import wall_pose
import numpy as np
import pypcd
from sklearn.cluster import DBSCAN
from sklearn.neighbors.nearest_centroid import NearestCentroid
import pprint
from geometry_msgs.msg import Pose
from robot.msg import Cluster_poses, Cluster_pose, Cat, Cats
from robot.srv import cloudSave, cloudProcess
import sys
import rosbag
# campus 68 166,206,227
# clothing_store 46 31,120,180
# corridor 54 178,223,138
# kitchen 108 51,160,44
# food_court 77 251,154,153
# lecture_room 44 227,26,28
# lobby 113 253,191,111
# office 129 255,127,0
# parking_garage 135 202,178,201
# restaurant 152 106,61,154
# supermarket 175 255,255,153
path = 'default'
clusters = Cluster_poses()
cats = []


def cloud_saver(pathS, msg, wfm, affix):
    global path
    if wfm:
        print('reading map')
        msg = rospy.wait_for_message('semantic_mapper/cloud', PointCloud2)
        print('map read')
        pc = pypcd.pypcd.PointCloud.from_msg(msg)
        path = sys.path[0]
        path = '%s/../resources/pointCloud%s.pcd' % (path, affix)
        pypcd.save_point_cloud_bin_compressed(pc, path)
    else:
        pc = pypcd.pypcd.PointCloud.from_msg(msg)
        pypcd.save_point_cloud_bin_compressed(pc, pathS)


def cloud_reader(path):
    pc = pypcd.pypcd.PointCloud.from_path(path)
    pc = pc.to_msg()
    pc.header.frame_id = 'map'
    return pc


def cloud_parser(msg):
    cat_reader()
    global cats

    data = pypcd.numpy_pc2.pointcloud2_to_array(
        msg, split_rgb=True, remove_padding=True)
    # print data
    X = np.zeros((len(data[0]), 3))
    Color = np.zeros((len(data[0]), 3))
    for x in range(len(cats.cats)):
        for i in range(len(data[0])):
            temp = [(data[0][i][3], data[0][i][4], data[0][i][5])]
            if temp[0][0] == cats.cats[x].color[0] and temp[0][1] == cats.cats[x].color[1] and temp[0][2] == cats.cats[x].color[2]:
                data[0][i][2] = x * 2 + 1
                X[i] = np.array([data[0][i][0], data[0][i][1],
                                 data[0][i][2]], dtype=np.float32)
                Color[i] = np.array(
                    [data[0][i][3], data[0][i][4], data[0][i][5]], dtype=np.uint8)

    X = X[~(X == 0).all(1)]
    Color = Color[~(Color == 0).all(1)]
    return X, Color


def cloud_clustering(X):
    clustering = DBSCAN(eps=0.8, min_samples=1000).fit_predict(X)
    return clustering


def wall_canceling(X,Color):

    print("waiting for message")
    msg = rospy.wait_for_message('wall_poses', wall_pose)
    index = list()
    # rows = []
    # X = np.zeros((len(msg.x),3))
    # Color = np.ones((len(msg.x),3), dtype=np.uint8)
    # for x in range(len(msg.x)):
    #     X[x] = np.array([msg.x[x], msg.y[x], 0], dtype=np.float32)
    for i in range(len(msg.x)):
        # rows, cols = np.where(X[:,0] > msg.x[i]-msg.res)
        for j in range(len(X)):
            if X[j][0] > msg.x[i]-msg.res and X[j][0] < msg.x[i]+msg.res and X[j][1] > msg.y[i]-msg.res and X[j][1] < msg.y[i]+msg.res:
                index.append(j)
        print i
    # return X, Color
    # print rows
    X_new = np.delete(X, index, axis=0)
    Color_new = np.delete(Color, index, axis=0)
    return X_new, Color_new


def noise_canceling(X, y, Color):
    index = list()
    for i in range(len(y)):
        if y[i] == -1:
            index.append(i)
    X_new = np.delete(X, index, axis=0)
    y_new = np.delete(y, index)
    Color_new = np.delete(Color, index, axis=0)
    return X_new, y_new, Color_new


def generate_Cluster_poses(centroids, pc, index):
    global clusters
    global cats

    for i in range(len(centroids)):
        cluster = Cluster_pose()

        pose_temp = Pose()
        pose_temp.position.x = centroids[i][0]
        pose_temp.position.y = centroids[i][1]
        pose_temp.position.z = centroids[i][2]
        pose_temp.orientation.x = 0
        pose_temp.orientation.y = 0
        pose_temp.orientation.z = 0
        pose_temp.orientation.w = 1
        cluster.centroid = pose_temp

        cluster.index = i

        points = np.zeros((len(index),3))
        for x in range(len(index)):
            if index[x] == i:
                points[x] = np.array([pc[x][0], pc[x][1], pc[x][2]], dtype=np.float32)

        points = points[~(points == 0).all(1)]

        for x in range(len(cats.cats)):
            if pose_temp.position.z == x * 2 + 1:
                cluster.label = cats.cats[x].label

        X = cloud_height_collapser(points)

        Color = np.zeros((len(points),3))
        encoded_colors = pypcd.encode_rgb_for_pcl((Color).astype(np.uint8))
        new_data = np.hstack((X[:, 0, np.newaxis].astype(np.float32), X[:, 1, np.newaxis].astype(
            np.float32), X[:, 2, np.newaxis].astype(np.float32), encoded_colors[:, np.newaxis]))

        new_cloud = pypcd.make_xyz_rgb_point_cloud(new_data)
        pprint.pprint(new_cloud.get_metadata())
        new_msg = new_cloud.to_msg()
        new_msg.header.frame_id = 'map'
        cluster.cloud = new_msg

        clusters.clusters.append(cluster)


def cluster_pose(centroids):
    global poses
    # pose_temp = Pose()
    for i in range(len(centroids)):
        pose_temp = Pose()
        pose_temp.position.x = centroids[i][0]
        pose_temp.position.y = centroids[i][1]
        pose_temp.position.z = centroids[i][2]
        pose_temp.orientation.x = 0
        pose_temp.orientation.y = 0
        pose_temp.orientation.z = 0
        pose_temp.orientation.w = 1
        poses.poses.append(pose_temp)


def cluster_labels():
    global poses
    global cats
    for i in range(len(poses.poses)):
        for x in range(len(cats.cats)):
            if poses.poses[i].position.z == x * 2 + 1:
                poses.labels.append(cats.cats[x].label)


def cloud_height_collapser(X):
    for i in range(len(X)):
        X[i][2] = 0
    return X


def cloud_save_service(req):
    if req.map:
        cloud_saver(None, None, req.map, req.affix)
    return True


def cloud_process_service(req):
    pc = PointCloud2()
    if req.map:
        print('reading map')
        msg = rospy.wait_for_message('semantic_mapper/cloud', PointCloud2)
        print('map read')
        pc = pypcd.pypcd.PointCloud.from_msg(msg)
        pc.header.frame_id = 'map'
    else:
        path = sys.path[0]
        path = '%s/../resources/pointCloud%s.pcd' % (path, req.affix)
        pc = cloud_reader(path)
    cloud_process(pc, req.affix)


def cloud_process(pc, affix):
    X, Color = cloud_parser(pc)
    X, y, Color = wall_canceling(X, y, Color)
    y = cloud_clustering(X)
    X, y, Color = noise_canceling(X, y, Color)

    centroids = NearestCentroid()
    centroids.fit(X, y)
    cluster_pose(centroids.centroids_)
    cluster_labels()
    global poses
    print poses

    bag_path = sys.path[0]
    bag_path = '%s/../resources/clusters%s.bag' % (bag_path, affix)

    bag = rosbag.Bag(bag_path, 'w')
    try:
        bag.write('cluster_centroids', poses)
    finally:
        bag.close()

    X = cloud_height_collapser(X)
    encoded_colors = pypcd.encode_rgb_for_pcl((Color).astype(np.uint8))
    new_data = np.hstack((X[:, 0, np.newaxis].astype(np.float32), X[:, 1, np.newaxis].astype(
        np.float32), X[:, 2, np.newaxis].astype(np.float32), encoded_colors[:, np.newaxis]))

    new_cloud = pypcd.make_xyz_rgb_point_cloud(new_data)
    pprint.pprint(new_cloud.get_metadata())
    new_msg = new_cloud.to_msg()
    new_msg.header.frame_id = 'map'

    path = sys.path[0]
    path = '%s/../resources/pointCloud_test_of new_thing.pcd' % (path)
    cloud_saver(path, new_msg, False, None)
    pub = rospy.Publisher('walls_reveived', wall_pose, queue_size=1)
    test = wall_pose()
    pub.publish(test)
    print 'done'


def cat_reader():
    path = sys.path[0]
    path = '%s/../resources/my_cats.txt' % path

    file1 = open(path, "r")

    list1 = []
    colorList = []
    lines = file1.readlines()
    for i in range(1, len(lines)):
        list1.append(lines[i].split())
    for i in range(len(list1)):
        colorList.append(list1[i][2].split(','))
    new_array = np.array(colorList).astype(np.uint8)
    global cats
    cats = Cats()
    # print new_array
    for i in range(len(list1)):
        cat = Cat()
        cat.label = list1[i][0]
        cat.color = new_array[i]
        print cat.color
        cats.cats.append(cat)


def main():
    rospy.init_node("Parse_Map")
    # rate = rospy.Rate(10)

    path = sys.path[0]
    path = '%s/../resources/pointCloud_new_testv2_processed.pcd' % (path)
    pc = cloud_reader(path)
    X, Color = cloud_parser(pc)
    # X, y, Color = wall_canceling(X, y, Color)
    y = cloud_clustering(X)

    X, y, Color = noise_canceling(X, y, Color)


    centroids = NearestCentroid()
    centroids.fit(X, y)
    generate_Cluster_poses(centroids.centroids_, X, y)
    global clusters

    bag_path = sys.path[0]
    bag_path = '%s/../resources/cluster_testv2_2.bag' % (bag_path)

    bag = rosbag.Bag(bag_path, 'w')
    try:
        bag.write('clusters', clusters)
    finally:
        bag.close()
    #
    # X = cloud_height_collapser(X)
    #
    # encoded_colors = pypcd.encode_rgb_for_pcl((Color).astype(np.uint8))
    # new_data = np.hstack((X[:, 0, np.newaxis].astype(np.float32), X[:, 1, np.newaxis].astype(
    #     np.float32), X[:, 2, np.newaxis].astype(np.float32), encoded_colors[:, np.newaxis]))
    #
    # new_cloud = pypcd.make_xyz_rgb_point_cloud(new_data)
    # pprint.pprint(new_cloud.get_metadata())
    # new_msg = new_cloud.to_msg()
    # new_msg.header.frame_id = 'map'
    #
    # path = sys.path[0]
    # path = '%s/../resources/pointCloud_with_wall_canceling_clustered.pcd' % (path)
    # cloud_saver(path, new_msg, False, None)
    # print("something")
    #
    # # pub = rospy.Publisher('wall_canceling_map', PointCloud2, queue_size=1)
    # # while not rospy.is_shutdown():
    # #     pub.publish(new_msg)
    # #     rate.sleep()
    #
    # rospy.Service('cloud_saver', cloudSave, cloud_save_service)
    # rospy.Service('cloud_process', cloudProcess, cloud_process_service)
    #
    # #### testing the system ####
    # # global path
    # # path = sys.path[0]
    # # path = '%s/../resources/pointCloud.pcd' % path
    # #
    # # X = cloud_reader(path)
    # # X, Color = cloud_parser(X)
    # # y = cloud_clustering(X)
    # # X, y, Color = noise_canceling(X, y, Color)
    # # centroids = NearestCentroid()
    # # centroids.fit(X, y)
    # # cluster_pose(centroids.centroids_)
    # # cluster_labels()
    # # global poses
    # # print poses
    #
    #
    #
    #
    # rospy.spin()
