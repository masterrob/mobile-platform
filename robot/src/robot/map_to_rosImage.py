import rospy
import cv2
import numpy as np
import matplotlib.pyplot as plt
from robot.msg import wall_pose
from nav_msgs.msg import OccupancyGrid
from geometry_msgs.msg import Pose

def received_CB(msg):
    global received
    received = True



def main():
    rospy.init_node('map_to_image_node')
    rate = rospy.Rate(10)

    pub = rospy.Publisher('wall_poses', wall_pose, queue_size=1)
    rospy.Subscriber('walls_received', wall_pose, received_CB)

    msg = rospy.wait_for_message('map', OccupancyGrid)

    width = msg.info.width
    height = msg.info.height
    res = msg.info.resolution
    origin = msg.info.origin

    map_data = msg.data

    new_image = np.zeros((width, height))

    for i in range(0,height-1):
        for j in range(0,width-1):
            if map_data[j+i*height] == -1:
                new_image[i][j] = 127
            if map_data[j+i*height] == 0:
                new_image[i][j] = 200
            if map_data[j+i*height] == 100:
                new_image[i][j] = 0


    ow = int(round(abs(origin.position.x)/res))
    oh = int(round(abs(origin.position.y)/res))

    img = np.array(new_image, dtype = np.uint8)
    image = np.flip(img, 0)
    kernelD = np.ones((21,1),np.uint8)
    kernelE = np.ones((21,1),np.uint8)
    eMap1 = cv2.erode(image,kernelE, iterations=1)
    dMap1 = cv2.dilate(eMap1,kernelD, iterations=1)
    kernel = np.ones((1,3), np.uint8)
    final = cv2.dilate(dMap1,kernel, iterations=1)
    kernel = np.ones((3,3), np.uint8)
    final = cv2.erode(dMap1,kernel, iterations=2)
    # # best_map = cv2.dilate(map, kernelD,iterations = 1)


    poses = wall_pose()
    poses.res = res
    final = np.flip(final, 0)
    for i in range(10,height-1-10):
        for j in range(10,width-1-10):
            if final[i][j] == 0:
                poses.x.append((j-ow)*res)
                poses.y.append((i-oh)*res)

    print len(poses.x)
    global received
    received = False

    cv2.imshow('map', final)
    k = cv2.waitKey(0)
    if k == 27:
        cv2.destroyAllWindows()

    # while not received:
    pub.publish(poses)
        # rate.sleep()
