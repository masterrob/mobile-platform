import rospy
from sensor_msgs.msg import Image

count = 1

def image_CB(msg):
    global pub
    global count

    if count == 6:
        pub.publish(msg)
        count = 1
    else:
        count += 1

def main():
    rospy.init_node('Camera_Limiter')
    global pub
    pub = rospy.Publisher('camera/limited', Image, queue_size=1)
    rospy.Subscriber('camera/color/image_raw', Image, image_CB)

    rospy.spin()
