#!/usr/bin/env python
# license removed for brevity

import rospy
import actionlib
import math
import numpy as np
import tf
import pypcd
from nav_msgs.msg import Odometry
from robot.msg import obj_loc, Cluster_poses
from std_msgs.msg import String
from geometry_msgs.msg import Pose, PoseArray
from visualization_msgs.msg import Marker, MarkerArray
from move_base_msgs.msg import MoveBaseAction, MoveBaseGoal
from object_detection.srv import completionYOLO


# nav_test = 1

def object_CB(msg):
    print("Something before everything")
    global nav_test
    place = msg.location
    clusters = rospy.wait_for_message('clusters', Cluster_poses)
    tf_listener = tf.TransformListener()
    tf_listener.waitForTransform(
        '/map', '/base_link', rospy.Time(), rospy.Duration(4))
    (trans, rot) = tf_listener.lookupTransform(
        '/map', '/base_link', rospy.Time())

    locations = list()
    for i in range(len(clusters.clusters)):
        temp = Pose()
        if clusters.clusters[i].label == place:
            temp = clusters.clusters[i].centroid
            locations.append(i)

    dist = list()
    for i in range(len(locations)):
        tempx = trans[0] - clusters.clusters[i].centroid.position.x
        tempy = trans[1] - clusters.clusters[i].centroid.position.y
        temp = math.sqrt(math.pow(tempx, 2) + math.pow(tempy, 2))
        dist.append(temp)

    index = np.argmin(dist)
    goal_cloud_index = locations[index]
    # goal_cloud_index = nav_test

    dist = list()
    cloud = pypcd.numpy_pc2.pointcloud2_to_array(
        clusters.clusters[goal_cloud_index].cloud, split_rgb=True, remove_padding=True)
    for i in range(len(cloud[0])):
        tempx = trans[0] - cloud[0][i][0]
        tempy = trans[1] - cloud[0][i][1]
        temp = math.sqrt(math.pow(tempx, 2) + math.pow(tempy, 2))
        dist.append(temp)

    index = np.argmin(dist)
    goal_pose = Pose()
    goal_pose.position.x = cloud[0][index][0]
    goal_pose.position.y = cloud[0][index][1]
    goal_pose.position.z = 0
    goal_pose.orientation.x = 0
    goal_pose.orientation.y = 0
    goal_pose.orientation.z = 0
    goal_pose.orientation.w = 1

    print("Sending goal")
    result = movebase_client(goal_pose)
    # YOLOservice = rospy.ServiceProxy('startDarknet', completionYOLO)
    # YOLOservice.call(msg.object)
    # print nav_test
    # if nav_test == 2:
    #     nav_test = 0
    # else:
    #     nav_test += 1
    print result




def goHomeRobot_CB(msg):

    if msg.data == "Home":
        home_pose = Pose()
        home_pose.position.x = 0
        home_pose.position.y = 0
        home_pose.position.z = 0

        home_pose.orientation.x = 0
        home_pose.orientation.y = 0
        home_pose.orientation.z = 0
        home_pose.orientation.w = 1
        print "going home"
        result = movebase_client(home_pose)


def movebase_client(goal_pose):

    client = actionlib.SimpleActionClient('move_base', MoveBaseAction)
    client.wait_for_server()
    goal = MoveBaseGoal()

    goal.target_pose.header.frame_id = "map"
    goal.target_pose.header.stamp = rospy.Time.now()
    goal.target_pose.pose = goal_pose
    goal.target_pose.pose.position.z = 0

    client.send_goal(goal)
    wait = client.wait_for_result()
    if not wait:
        rospy.logerr("Action server not available!")
        rospy.signal_shutdown("Action server not available!")
    else:
        return client.get_result()


def main():
    rospy.init_node('movebase_client_py')
    rospy.Subscriber('object_location', obj_loc, object_CB)
    rospy.Subscriber('goHomeRobot', String, goHomeRobot_CB)

    rospy.spin()
