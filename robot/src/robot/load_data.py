import csv
import itertools
import sys
import rospy
from robot.srv import database
from robot.msg import obj_loc

# Load data from file and reshape
data = list()


def read_data():
    global data
    path = sys.path[0]
    path = '%s/../resources/data.csv' % (path)

    with open(path) as csvfile:
        data = list(csv.reader(csvfile))

    data = list(map(list, itertools.izip_longest(*data)))


def objectToPlace(obj_in):
    global data
    for rooms in data:
        # print(rooms[0])
        for objects in rooms:
            if objects:
                # print(objects)
                if objects == obj_in:
                    place_out = rooms[0]
    return place_out


def service_CB(req):
    global pub
    obj = req.input
    place = objectToPlace(obj)
    temp = obj_loc()
    temp.object = obj
    temp.location = place
    pub.publish(temp)

    return place


def main():
    rospy.init_node('Database_node')
    global pub
    pub = rospy.Publisher('object_location', obj_loc, queue_size=1)
    read_data()

    rospy.Service('Database', database, service_CB)

    rospy.spin()
