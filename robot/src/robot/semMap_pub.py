import rospy
import pypcd
from sensor_msgs.msg import PointCloud2
import sys


def main():
    rospy.init_node('semantic_map_node')
    rate = rospy.Rate(10)

    raw_pub = rospy.Publisher('raw_semMap', PointCloud2, queue_size=1)
    proc_pub = rospy.Publisher('processed_semMap', PointCloud2, queue_size=1)

    path = sys.path[0]
    path = '%s/../resources/pointCloud_testv2.pcd' % (path)

    raw_msg = pypcd.pypcd.PointCloud.from_path(path)
    raw_msg = raw_msg.to_msg()
    raw_msg.header.frame_id = 'map'

    path = sys.path[0]
    path = '%s/../resources/pointCloud_new_testv2_processed.pcd' % (path)

    proc_msg = pypcd.pypcd.PointCloud.from_path(path)
    proc_msg = proc_msg.to_msg()
    proc_msg.header.frame_id = 'map'

    while not rospy.is_shutdown():
        raw_pub.publish(raw_msg)
        proc_pub.publish(proc_msg)
        rate.sleep()
