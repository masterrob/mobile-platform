import rospy
from sensor_msgs.msg import LaserScan

laser = rospy.Publisher('laser', LaserScan, queue_size=1)


def scan_CB(msg):
    global laser
    laser.publish(msg)


def main():
    rospy.init_node('Republisher')
    rospy.Subscriber('scan', LaserScan, scan_CB)

    rospy.spin()
