import rospy
import sys
import rosbag

from geometry_msgs.msg import Pose
from sensor_msgs.msg import PointCloud2
from robot.msg import Cluster_poses
from visualization_msgs.msg import Marker, MarkerArray


def readBag():
    path = sys.path[0]
    path = '%s/../resources/cluster_testv2_2.bag' % (path)

    bag = rosbag.Bag(path)

    clusters = Cluster_poses()

    for topic, msg, t in bag.read_messages():
        clusters = msg
    bag.close()

    return clusters


def createArrayA(clusters):
    id = 0
    markerArray = MarkerArray()
    for i in range(len(clusters.clusters)):
        marker = Marker()
        marker.header.stamp = rospy.Time.now()
        marker.header.frame_id = 'map'
        marker.type = 2
        marker.id = id

        marker.pose = clusters.clusters[i].centroid
        marker.pose.position.z = 0

        marker.color.a = 0.7
        marker.color.r = 0.0
        marker.color.g = 1.0
        marker.color.b = 0.0
        marker.scale.x = 0.5
        marker.scale.y = 0.5
        marker.scale.z = 0.5

        markerArray.markers.append(marker)
        id += 1
    return markerArray


def createArrayB(clusters):
    id = 0
    markerArray = MarkerArray()
    for i in range(len(clusters.clusters)):
        marker = Marker()
        marker.header.stamp = rospy.Time.now()
        marker.header.frame_id = 'map'
        marker.type = 9
        marker.id = id

        marker.pose = clusters.clusters[i].centroid
        marker.pose.position.z = 1

        marker.color.a = 1.0
        marker.color.r = 0.0
        marker.color.g = 0.0
        marker.color.b = 0.0
        marker.scale.z = 1.0
        marker.text = clusters.clusters[i].label

        markerArray.markers.append(marker)
        id += 1
    return markerArray


def main():
    rospy.init_node('Cluster_markers_node')
    rate = rospy.Rate(1)
    marker_pub = rospy.Publisher('cluster_markers', MarkerArray, queue_size=1)
    cluster_pub = rospy.Publisher('clusters', Cluster_poses, queue_size=1)
    markerText_pub = rospy.Publisher(
        'cluster_markers_text', MarkerArray, queue_size=1)

    clusters = readBag()



    while not rospy.is_shutdown():
        markerArray = createArrayA(clusters)
        marker_pub.publish(markerArray)
        markerArrayText = createArrayB(clusters)
        markerText_pub.publish(markerArrayText)
        cluster_pub.publish(clusters)
        rate.sleep()
