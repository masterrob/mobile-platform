#
# Copyright 1993-2019 NVIDIA Corporation.  All rights reserved.
#
# NOTICE TO LICENSEE:
#
# This source code and/or documentation ("Licensed Deliverables") are
# subject to NVIDIA intellectual property rights under U.S. and
# international Copyright laws.
#
# These Licensed Deliverables contained herein is PROPRIETARY and
# CONFIDENTIAL to NVIDIA and is being provided under the terms and
# conditions of a form of NVIDIA software license agreement by and
# between NVIDIA and Licensee ("License Agreement") or electronically
# accepted by Licensee.  Notwithstanding any terms or conditions to
# the contrary in the License Agreement, reproduction or disclosure
# of the Licensed Deliverables to any third party without the express
# written consent of NVIDIA is prohibited.
#
# NOTWITHSTANDING ANY TERMS OR CONDITIONS TO THE CONTRARY IN THE
# LICENSE AGREEMENT, NVIDIA MAKES NO REPRESENTATION ABOUT THE
# SUITABILITY OF THESE LICENSED DELIVERABLES FOR ANY PURPOSE.  IT IS
# PROVIDED "AS IS" WITHOUT EXPRESS OR IMPLIED WARRANTY OF ANY KIND.
# NVIDIA DISCLAIMS ALL WARRANTIES WITH REGARD TO THESE LICENSED
# DELIVERABLES, INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY,
# NONINFRINGEMENT, AND FITNESS FOR A PARTICULAR PURPOSE.
# NOTWITHSTANDING ANY TERMS OR CONDITIONS TO THE CONTRARY IN THE
# LICENSE AGREEMENT, IN NO EVENT SHALL NVIDIA BE LIABLE FOR ANY
# SPECIAL, INDIRECT, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, OR ANY
# DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
# WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS
# ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE
# OF THESE LICENSED DELIVERABLES.
#
# U.S. Government End Users.  These Licensed Deliverables are a
# "commercial item" as that term is defined at 48 C.F.R. 2.101 (OCT
# 1995), consisting of "commercial computer software" and "commercial
# computer software documentation" as such terms are used in 48
# C.F.R. 12.212 (SEPT 1995) and is provided to the U.S. Government
# only as a commercial end item.  Consistent with 48 C.F.R.12.212 and
# 48 C.F.R. 227.7202-1 through 227.7202-4 (JUNE 1995), all
# U.S. Government End Users acquire the Licensed Deliverables with
# only those rights set forth herein.
#
# Any use of the Licensed Deliverables in individual and commercial
# software must include, in the user documentation and internal
# comments to the code, the above Disclaimer and U.S. Government End
# Users Notice.
#

# This sample uses a Caffe ResNet50 Model to create a TensorRT Inference Engine
import rospy
import random
from PIL import Image
from sensor_msgs.msg import Image as RosImage
from robot.srv import prediction
import numpy as np
import time
from cv_bridge import CvBridge
import cv2
import matplotlib.pyplot as plt

import pycuda.driver as cuda
# This import causes pycuda to automatically manage CUDA context creation and cleanup.
import pycuda.autoinit

import tensorrt as trt

import sys, os
sys.path.insert(1, os.path.join('/usr/src/tensorrt/samples/python'))
import common

class ModelData(object):
    MODEL_PATH = "/home/jacob/Documents/workspaces/mobile_robot_ws/src/mobile-platform/robot/resources/places.caffemodel"
    DEPLOY_PATH = "/home/jacob/Documents/workspaces/mobile_robot_ws/src/mobile-platform/robot/resources/deploy3.prototxt"
    MEAN_PATH = "/home/jacob/Documents/workspaces/mobile_robot_ws/src/mobile-platform/robot/resources/places205CNN_mean.binaryproto"
    INPUT_SHAPE = (3, 227, 227)
    OUTPUT_NAME = "prob"
    # We can convert TensorRT data types to numpy types with trt.nptype()
    DTYPE = trt.float32

# You can set the logger severity higher to suppress messages (or lower to display more messages).
TRT_LOGGER = trt.Logger(trt.Logger.WARNING)


def load_normalized_test_case(test_image, pagelocked_buffer):
    # Converts the input image to a CHW Numpy array
    def normalize_image(image):
        # Resize, antialias and transpose the image to CHW.
        c, h, w = ModelData.INPUT_SHAPE
        return np.asarray(image.resize((w, h), Image)).transpose([2, 0, 1]).astype(trt.nptype(ModelData.DTYPE)).ravel()

    # Normalize the image and copy to pagelocked memory.
    np.copyto(pagelocked_buffer, normalize_image(test_image))
    return test_image


class NeuralNetwork():
    def __init__(self):
        rospy.Service('NN_service', prediction, self.NN_service)
        # Set the data path to the directory that contains the trained models and test images for inference.
        _, self.data_files = common.find_sample_data(description="Runs a ResNet50 network with a TensorRT inference engine.", subfolder="resnet50", find_files=["binoculars.jpeg", "reflex_camera.jpeg", "tabby_tiger_cat.jpg", ModelData.MODEL_PATH, ModelData.DEPLOY_PATH, "/home/jacob/Documents/workspaces/mobile_robot_ws/src/mobile-platform/robot/resources/categoryIndex_places205.csv"])
        # Get test images, models and labels.
        self.test_images = self.data_files[0:3]
        caffe_model_file, caffe_deploy_file, labels_file = self.data_files[3:]
        self.labels = open(labels_file, 'r').read().split('\n')
        # Build a TensorRT engine.
        self.engine = self.build_engine_caffe(caffe_model_file, caffe_deploy_file)
            # Inference is the same regardless of which parser is used to build the engine, since the model architecture is the same.
            # Allocate buffers and create a CUDA stream.
        self.h_input, self.d_input, self.h_output, self.d_output, self.stream = self.allocate_buffers(self.engine)
        self.context = self.engine.create_execution_context()
        self.inference = False
        self.bridge = CvBridge()
        self.mean = self.retrieve_mean(ModelData.MEAN_PATH)
        print("something")


    def allocate_buffers(self, engine):
        # Determine dimensions and create page-locked memory buffers (i.e. won't be swapped to disk) to hold host inputs/outputs.
        h_input = cuda.pagelocked_empty(trt.volume(engine.get_binding_shape(0)), dtype=trt.nptype(ModelData.DTYPE))
        h_output = cuda.pagelocked_empty(trt.volume(engine.get_binding_shape(1)), dtype=trt.nptype(ModelData.DTYPE))
        # Allocate device memory for inputs and outputs.
        d_input = cuda.mem_alloc(h_input.nbytes)
        d_output = cuda.mem_alloc(h_output.nbytes)
        # Create a stream in which to copy inputs/outputs and run inference.
        stream = cuda.Stream()
        return h_input, d_input, h_output, d_output, stream

    def do_inference(self, context, h_input, d_input, h_output, d_output, stream):
        # Transfer input data to the GPU.
        cuda.memcpy_htod_async(d_input, h_input, stream)
        # Run inference.
        context.execute_async(bindings=[int(d_input), int(d_output)], stream_handle=stream.handle)
        # Transfer predictions back from the GPU.
        cuda.memcpy_dtoh_async(h_output, d_output, stream)
        # Synchronize the stream
        stream.synchronize()

    def retrieve_mean(self, mean_proto):
        with trt.CaffeParser() as parser:
            return parser.parse_binary_proto(mean_proto)

    def build_engine_caffe(self, model_file, deploy_file):
        # You can set the logger severity higher to suppress messages (or lower to display more messages).
        with trt.Builder(TRT_LOGGER) as builder, builder.create_network() as network, trt.CaffeParser() as parser:
            # Workspace size is the maximum amount of memory available to the builder while building an engine.
            # It should generally be set as high as possible.
            builder.max_workspace_size = common.GiB(1)
            # Load the Caffe model and parse it in order to populate the TensorRT network.
            # This function returns an object that we can query to find tensors by name.
            model_tensors = parser.parse(deploy=deploy_file, model=model_file, network=network, dtype=ModelData.DTYPE)
            # For Caffe, we need to manually mark the output of the network.
            # Since we know the name of the output tensor, we can find it in model_tensors.
            network.mark_output(model_tensors.find(ModelData.OUTPUT_NAME))
            return builder.build_cuda_engine(network)

    def prediction(self):
        incomingImage = rospy.wait_for_message("/camera/color/image_raw", RosImage)
        img = self.bridge.imgmsg_to_cv2(incomingImage, "bgr8")
        # img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
        # im_pil = Image.fromarray(img)
        # im_pil = load_normalized_test_case(im_pil, self.h_input)
        img = cv2.resize(img, (227,227))
        self.h_input = cv2.normalize(img,  self.h_input, 0, 1, cv2.NORM_HAMMING)

        # self.test_case = load_normalized_test_case(self.test_image, self.h_input)
        # self.h_input, self.d_input, self.h_output, self.d_output, self.stream = allocate_buffers(self.engine)
        self.do_inference(self.context, self.h_input, self.d_input, self.h_output, self.d_output, self.stream)
        pred = self.labels[np.argmax(self.h_output)]
        # temp = np.argsort(self.h_output)[-5:][::-1]
        labels_test = [self.labels[43], self.labels[44], self.labels[45]]
        output_test = [self.h_output[43], self.h_output[44], self.h_output[45]]

        for i in range(0,3):
            output_test[i] = output_test[i] / sum(output_test)
            print(output_test[i])

        test = labels_test[np.argmax(output_test)]
        print("Recognized image as: " + test)
        self.inference = False
        return pred, 0.5

    def NN_service(self, req):
        self.inference = True



def main():
    rospy.init_node("shiiiiiii")

    rate = rospy.Rate(1000)
    ne = NeuralNetwork()
    while not rospy.is_shutdown():
        if ne.inference:
            ne.prediction()
        else:
            rospy.sleep(0.001)


if __name__ == '__main__':
    main()
