import sys
import rospy
import rviz
import cv2
import numpy as np
import csv
import itertools
from python_qt_binding.QtWidgets import QWidget, QApplication, QMainWindow, QListWidget, QPushButton, QGridLayout, QLabel, QRadioButton, QVBoxLayout, QCheckBox, QHBoxLayout
from python_qt_binding.QtGui import *
from python_qt_binding.QtCore import *
from robot.srv import database
from std_msgs.msg import String
from sensor_msgs.msg import CompressedImage


class Worker(QThread):

    def __init__(self):
        QThread.__init__(self)

    def run(self):
        print("run")


class App(QMainWindow):

    def __init__(self):
        super(App, self).__init__()
        self.title = 'Semantic map GUI'
        self.left = 100
        self.top = 100
        self.width = 800
        self.height = 800

        self.data = list()

        self.frame = rviz.VisualizationFrame()
        self.frame.initialize()
        self.frame.setMenuBar(None)
        self.frame.setStatusBar(None)
        self.frame.setHideButtonVisibility(False)
        self.manager = self.frame.getManager()

        self.Qimg = QImage()
        self.label = QLabel()
        self.pixmap = QPixmap()

        rospy.Subscriber("/camera/color/image_raw/compressed",
                         CompressedImage, self.Image_callback)
        self.home_pub = rospy.Publisher('goHomeRobot', String, queue_size=1)
        self.databaseService = rospy.ServiceProxy('Database', database)

        self.reader = rviz.YamlConfigReader()
        self.config = rviz.Config()

        path = sys.path[0]
        gui_path = '%s/../resources/Gui.rviz' % (path)
        self.reader.readFile(self.config, gui_path)
        self.frame.load(self.config)

        self.amcl_display = self.manager.getRootDisplayGroup().getDisplayAt(7)
        self.local_display = self.manager.getRootDisplayGroup().getDisplayAt(8)
        self.global_display = self.manager.getRootDisplayGroup().getDisplayAt(9)

        self.raw_display = self.manager.createDisplay(
            "rviz/PointCloud2", "Raw_point_cloud", False)
        # self.raw_display.subProp("Topic").setValue("/raw_semMap")
        self.raw_display.subProp("Topic").setValue("/semantic_mapper/cloud")
        self.raw_display.subProp("Size (m)").setValue(0.1)

        self.proc_display = self.manager.createDisplay(
            "rviz/PointCloud2", "Processed_point_cloud", False)
        self.proc_display.subProp("Topic").setValue("/processed_semMap")
        self.proc_display.subProp("Size (m)").setValue(0.1)

        self.marker_display = self.manager.createDisplay(
            "rviz/MarkerArray", "Marker", False)
        self.marker_display.subProp(
            "Marker Topic").setValue("/cluster_markers")

        self.marker_text_display = self.manager.createDisplay(
            "rviz/MarkerArray", "Marker_text", False)
        self.marker_text_display.subProp(
            "Marker Topic").setValue("/cluster_markers_text")

        self._worker = Worker()
        self._worker.start()
        self.initUI()

    def initUI(self):
        self.setWindowTitle(self.title)
        self.setGeometry(self.left, self.top, self.width, self.height)
        self.setWindowState(Qt.WindowMaximized)

        self.setCentralWidget(QWidget(self))

        self.checklayout = QVBoxLayout()

        self.layout = QGridLayout()
        self.layout.setColumnStretch(1, 1)
        self.layout.setColumnStretch(2, 1)
        self.layout.setColumnStretch(3, 3)

        self.Select_button = QPushButton()
        self.Select_button.setText("Confirm Selection")
        self.Select_button.clicked.connect(self.Butt_clicked)

        self.Home_button = QPushButton()
        self.Home_button.setText("Go Home")
        self.Home_button.clicked.connect(self.home_clicked)

        self.button_layout = QHBoxLayout()
        self.button_layout.addWidget(self.Select_button)
        self.button_layout.addWidget(self.Home_button)

        self.list = QListWidget()
        self.read_data()
        self.create_list()

        self.checkbox = QCheckBox("Default")
        self.checkbox.setChecked(True)
        self.checkbox.toggled.connect(self.onClicked)
        self.checkbox.setFixedWidth(100)
        self.checklayout.addWidget(self.checkbox)

        self.checkbox = QCheckBox("Raw")
        self.checkbox.toggled.connect(self.onClicked)
        self.checkbox.setFixedWidth(100)
        self.checklayout.addWidget(self.checkbox)

        self.checkbox = QCheckBox("Clusters")
        self.checkbox.toggled.connect(self.onClicked)
        self.checkbox.setFixedWidth(100)
        self.checklayout.addWidget(self.checkbox)

        self.checkbox = QCheckBox("Markers")
        self.checkbox.toggled.connect(self.onClicked)
        self.checkbox.setFixedWidth(100)
        self.checklayout.addWidget(self.checkbox)

        self.layout.addLayout(self.checklayout, 1, 2)
        self.layout.addWidget(self.list, 1, 1)
        self.layout.addWidget(self.label, 3, 1, 3, 2)
        self.layout.addLayout(self.button_layout, 2, 1)
        self.layout.addWidget(self.frame, 1, 3, 6, 4)

        self.centralWidget().setLayout(self.layout)

        self.show()

    def Butt_clicked(self):
        print(self.list.currentItem().text())
        temp = self.list.currentItem().text()
        # rospy.wait_for_service('NN_service')
        response = self.databaseService.call(temp)

    def home_clicked(self):
        temp = String()
        temp.data = "Home"
        self.home_pub.publish(temp)

    def Image_callback(self, msg):
        print("faggot")
        temp = np.fromstring(msg.data, np.uint8)
        Image = cv2.imdecode(temp, 1)
        Image = cv2.cvtColor(Image, cv2.COLOR_BGR2RGB)
        height, width, channel = Image.shape
        self.Qimg = QImage(Image, width, height, 3 *
                           width, QImage.Format_RGB888)
        self.pixmap = QPixmap(self.Qimg)
        self.label.setPixmap(self.pixmap)

    def onClicked(self):

        self.checkbox = self.sender()

        if self.checkbox.text() == "Default":
            self.amcl_display.setEnabled(self.checkbox.isChecked())
            self.local_display.setEnabled(self.checkbox.isChecked())
            self.global_display.setEnabled(self.checkbox.isChecked())

        if self.checkbox.text() == "Raw":
            self.raw_display.setEnabled(self.checkbox.isChecked())

        if self.checkbox.text() == "Clusters":
            self.proc_display.setEnabled(self.checkbox.isChecked())

        if self.checkbox.text() == "Markers":
            self.marker_display.setEnabled(self.checkbox.isChecked())
            self.marker_text_display.setEnabled(self.checkbox.isChecked())

    def read_data(self):
        path = sys.path[0]
        path = '%s/../resources/data.csv' % (path)

        with open(path) as csvfile:
            self.data = list(csv.reader(csvfile))
        self.data = list(map(list, itertools.izip_longest(*self.data)))

    def create_list(self):

        sorted_list = list()

        for rooms in self.data:
            # print(rooms[0])
            for objects in range(1, len(rooms)):
                if rooms[objects]:
                    sorted_list.append(rooms[objects])
                    # self.list.addItem(rooms[objects])
                    print(rooms[objects])

        sorted_list.sort()

        for i in sorted_list:
            self.list.addItem(i)


def main():
    rospy.init_node("Gui_node")
    app = QApplication(sys.argv)
    ex = App()
    sys.exit(app.exec_())
