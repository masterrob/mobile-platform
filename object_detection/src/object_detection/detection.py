# ROS
import rospy
import roslaunch
import tf
import sys
# from robot.msg import obj_loc
from object_detection.srv import completionYOLO
from object_detection.msg import objectLocation
# Python
import numpy as np
# Sensor messages
import sensor_msgs.point_cloud2 as pc2
from darknet_ros_msgs.msg import BoundingBoxes, BoundingBox
from sensor_msgs.msg import Image, PointCloud2


# Compute the position of the object in 3D using the point cloud.


def get_point(x, y):
    point = np.array([x, y, 0])
    msg = rospy.wait_for_message(
        "/camera/depth_registered/points", PointCloud2)
    depth = pc2.read_points(msg, field_names=(
        "x", "y", "z"), skip_nans=True, uvs=[(x, y)])  # Questionable
    cam_point = list(depth)
    break1 = False
    if(cam_point == []):
        for x_point in range(-5, 5):
            for y_point in range(-5, 5):
                tempx = x + x_point
                tempy = y + y_point
                depth = pc2.read_points(msg, field_names=("x", "y", "z"), skip_nans=True, uvs=[
                                        (tempx, tempy)])  # Questionable
                cam_point = list(depth)
                # print cam_point
                if(cam_point != []):
                    break1 = True
                    break
            if break1:
                break

    if(cam_point != []):
        point = np.array([cam_point[0][0], cam_point[0][1], cam_point[0][2]])
        print("point: ", cam_point)
        # publish_object_location(point)

    tf_listener = tf.TransformListener()
    tf_listener.waitForTransform(
        '/map', '/camera_color_optical_frame', rospy.Time(), rospy.Duration(4))
    (trans, rot) = tf_listener.lookupTransform(
        '/map', '/camera_color_optical_frame', rospy.Time())

    world_to_cam = tf.transformations.compose_matrix(
        translate=trans, angles=tf.transformations.euler_from_quaternion(rot))
    obj_vector = np.concatenate((point, np.ones(1))).reshape((4, 1))
    obj_base = np.dot(world_to_cam, obj_vector)
    print(obj_base[0:3])



# Calculates centroid of object of interest.
def calculate_centroid(x0, x1, y0, y1):
    xcenter = x0 + (x1 - x0) / 2
    ycenter = y0 + (y1 - y0) / 2
    get_point(xcenter, ycenter)


# ------------------------------- ROS STUFF ------------------------------------

# ROS subscriber callback and check for existence of objects based on probability, and the class we're looking for.
def data_callback(msg):
    global classInput
    global object_found
    global launch
    global startYOLO

    for bounding_box in msg.bounding_boxes:
        if bounding_box.probability > 0.4 and bounding_box.Class == classInput:
            launch.shutdown()
            text = '%s Found' % (classInput)
            print(text)
            object_found = True
            startYOLO = False
            calculate_centroid(bounding_box.xmin, bounding_box.xmax,
                               bounding_box.ymin, bounding_box.ymax)

        else:
            print("No object found")
#
#
def check_for_YOLO(req):
    global rate
    global startYOLO
    startYOLO = True

    global classInput
    classInput = req.label
    global object_found
    object_found = False


    return True
    # while not object_found:
    #     rate.sleep()





#and msg.bounding_boxes[0].Class == classInput
def main():
    rospy.init_node('object_detection_node', anonymous=True)
    global rate
    rate = rospy.Rate(10)
    rospy.Subscriber("/darknet_ros/bounding_boxes",
                     BoundingBoxes, data_callback)
    # rospy.Subscriber("/robot/object", obj_loc, user_input_callback)
    rospy.Service('startDarknet', completionYOLO, check_for_YOLO)
    global startYOLO
    startYOLO = False

    uuid = roslaunch.rlutil.get_or_generate_uuid(None, False)
    roslaunch.configure_logging(uuid)
    global launch
    path = sys.path[0]
    path = '%s/../../robot/launch/darknet_ros.launch' % (path)
    print(path)
    launch = roslaunch.parent.ROSLaunchParent(uuid, [path])
    isFirst = 1

    print "ready!"
    while not rospy.is_shutdown():
        if startYOLO and isFirst == 1:
            launch.start()
            rospy.loginfo("started")
            isFirst = 0
        elif not startYOLO:
            launch = roslaunch.parent.ROSLaunchParent(uuid, [path])
            isFirst = 1

        rate.sleep()

# ------------------------------------------------------------------------------
