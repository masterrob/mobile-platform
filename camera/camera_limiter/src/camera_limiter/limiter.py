import rospy
from sensor_msgs.msg import Image

counter = 0
pub = rospy.Publisher('image_raw', Image, queue_size=1)


def limiter_callback(msg):
    global counter

    if counter == 15:
        pub.publish(msg)
        counter = 0
    counter += 1


def main():
    rospy.init_node('vision_service_server', anonymous=True)
    rospy.Subscriber("camera/color/image_raw", Image, limiter_callback)


    print "ready!"
    rospy.spin()
